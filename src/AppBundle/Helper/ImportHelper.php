<?php

namespace AppBundle\Helper;

use AppBundle\Result\Result;
use AppBundle\Writer\DoctrineWriter;
use Ddeboer\DataImport\Reader\CsvReader;
use Ddeboer\DataImport\Step\MappingStep;
use Ddeboer\DataImport\Step\ValueConverterStep;
use Ddeboer\DataImport\Workflow\StepAggregator;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ImportHelper
 * @package AppBundle\Helper
 */
class ImportHelper
{
    /**
     * @var \SplFileObject
     */
    protected $file;

    /**
     * @var bool
     */
    protected $test;

    /**
     * @var array
     */
    protected $propertyMap = [
        'Product Name' => 'name',
        'Product Code' => 'code',
        'Product Description' => 'description',
        'Stock' => 'stockCount',
        'Cost in GBP' => 'cost',
        'Discontinued' => 'discontinuedAt',
    ];

    /**
     * @var Registry
     */
    protected $doctrine;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * ImportHelper constructor.
     * @param ValidatorInterface $validator
     * @param Registry $doctrine
     */
    public function __construct(ValidatorInterface $validator, Registry $doctrine)
    {
        $this->validator = $validator;
        $this->doctrine = $doctrine;
    }

    /**
     * @return \SplFileObject
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param \SplFileObject $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return bool
     */
    public function getTest()
    {
        return $this->test;
    }

    /**
     * @param bool $test
     */
    public function setTest($test)
    {
        $this->test = $test;
    }

    /**
     * @return Registry
     */
    public function getDoctrine()
    {
        return $this->doctrine;
    }

    /**
     * @param Registry $doctrine
     */
    public function setDoctrine($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @return ValidatorInterface
     */
    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * @param ValidatorInterface $validator
     */
    public function setValidator($validator)
    {
        $this->validator = $validator;
    }

    /**
     * @return array
     */
    public function getPropertyMap()
    {
        return $this->propertyMap;
    }

    /**
     * @param array $propertyMap
     */
    public function setPropertyMap($propertyMap)
    {
        $this->propertyMap = $propertyMap;
    }

    /**
     * Run workflow
     */
    public function process()
    {
        if (!$this->file instanceof \SplFileObject) {
            throw new \InvalidArgumentException("File is required");
        }

        $csvReader = new CsvReader($this->file);
        $csvReader->setHeaderRowNumber(0);

        $doctrineWriter = $this->getDoctrineWriter();
        $workflow = (new StepAggregator($csvReader))
            ->setSkipItemOnFailure(true)
            ->addStep($this->getMappingStep())
            ->addStep($this->getConventStep())
            ->addWriter($doctrineWriter);

        return new Result($workflow->process(), $doctrineWriter->getFailedItems());
    }


    /**
     * Returns a mapping step.
     * Used for rename properties
     *
     * @return MappingStep
     */
    protected function getMappingStep()
    {
        $map = [];
        foreach ($this->propertyMap as $key => $value) {
            $key = '[' . trim($key, '[]') . ']';
            $value = '[' . trim($value, '[]') . ']';
            $map[$key] = $value;
        };
        return new MappingStep($map);
    }

    /**
     * Returns a convent step
     * Used for convent value (change value or type)
     *
     * @return ValueConverterStep
     */
    public function getConventStep()
    {
        return (new ValueConverterStep())->add('[discontinuedAt]', function ($value) {
            if ($value && mb_strtolower($value) === 'yes') {
                return new \DateTime(date('Y-m-d H:i:s'));
            }
            return null;
        });
    }

    /**
     * @return DoctrineWriter
     */
    protected function getDoctrineWriter()
    {
        $writer = new DoctrineWriter($this->getDoctrine()->getManager(), 'AppBundle:Product', null, $this->getValidator());
        $writer->setInsert($this->getTest());
        return $writer;
    }

}