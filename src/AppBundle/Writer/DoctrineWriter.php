<?php

namespace AppBundle\Writer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class DoctrineWriter
 * @package AppBundle\Writer
 *
 * Override; main issue to validate entity before persist
 */
class DoctrineWriter extends \Ddeboer\DataImport\Writer\DoctrineWriter
{
    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var int
     */
    protected $transactionSize = 100;

    /**
     * @var int
     */
    protected $currentCount = 0;

    /**
     * @var array
     */
    protected $failedItems = [];

    /**
     * @return array
     */
    public function getFailedItems()
    {
        return $this->failedItems;
    }

    /**
     * @var bool
     */
    protected $insert = true;

    /**
     * @return bool
     */
    public function isInsert()
    {
        return $this->insert;
    }

    /**
     * @param bool $insert
     */
    public function setInsert($insert)
    {
        $this->insert = $insert;
    }

    /**
     * @inheritdoc
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManagerInterface $entityManager, $entityName, $index = null, ValidatorInterface $validator, $transactionSize = 100)
    {
        parent::__construct($entityManager, $entityName, $index);
        $this->validator = $validator;
    }


    /**
     * {@inheritdoc}
     */
    public function writeItem(array $item)
    {
        $entity = $this->findOrCreateItem($item);

        $this->loadAssociationObjectsToEntity($item, $entity);
        $this->updateEntity($item, $entity);

        if (0 === count($this->validator->validate($entity))) {
            if ($this->insert) {
                $this->entityManager->persist($entity);

                if (++$this->currentCount >= $this->transactionSize) {
                    $this->currentCount = 0;
                    $this->flush();
                }
            }
        } else {
            $this->failedItems[] = $item;
        }
    }
}