<?php

namespace AppBundle\Tests\Helper;

use AppBundle\Entity\Product;
use AppBundle\Helper\ImportHelper;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;

class ImportHelperTest extends KernelTestCase
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        self::bootKernel();

        $this->container = self::$kernel->getContainer();
    }

    /**
     * Test validation code
     */
    public function testCodeValidation()
    {
        $product = new Product();

        $validator = $this->container->get('validator');
        /** @var \Symfony\Component\Validator\ConstraintViolationList $violations */
        $violations = $validator->validateProperty($product, 'code');

        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("This value should not be blank.", $violations->get(0)->getMessage());

        $product->setCode('');
        $violations = $validator->validateProperty($product, 'code');
        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("This value should not be blank.", $violations->get(0)->getMessage());

        $product->setCode('12345678910');
        $this->assertGreaterThan(0, $violations->count());
        $violations = $validator->validateProperty($product, 'code');
        $this->assertSame("This value is too long. It should have 10 characters or less.", $violations->get(0)->getMessage());

        $product->setCode('P0001');
        $violations = $validator->validateProperty($product, 'code');
        $this->assertEquals(0, $violations->count());
    }

    /**
     * Test validation name
     */
    public function testNameValidation()
    {
        $product = new Product();

        $validator = $this->container->get('validator');
        /** @var \Symfony\Component\Validator\ConstraintViolationList $violations */
        $violations = $validator->validateProperty($product, 'name');

        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("This value should not be blank.", $violations->get(0)->getMessage());

        $product->setName('');
        $violations = $validator->validateProperty($product, 'name');
        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("This value should not be blank.", $violations->get(0)->getMessage());

        $product->setName(str_repeat('a', 256));
        $violations = $validator->validateProperty($product, 'name');
        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("This value is too long. It should have 255 characters or less.", $violations->get(0)->getMessage());

        $product->setName('Product name');
        $violations = $validator->validateProperty($product, 'name');
        $this->assertEquals(0, $violations->count());
    }

    /**
     * Test validation description
     */
    public function testDescriptionValidation()
    {
        $product = new Product();

        $validator = $this->container->get('validator');
        /** @var \Symfony\Component\Validator\ConstraintViolationList $violations */
        $violations = $validator->validateProperty($product, 'description');

        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("This value should not be blank.", $violations->get(0)->getMessage());

        $product->setDescription('');
        $violations = $validator->validateProperty($product, 'description');
        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("This value should not be blank.", $violations->get(0)->getMessage());

        $product->setDescription(str_repeat('a', 256));
        $violations = $validator->validateProperty($product, 'description');
        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("This value is too long. It should have 255 characters or less.", $violations->get(0)->getMessage());


        $product->setDescription('Product description');
        $violations = $validator->validateProperty($product, 'description');
        $this->assertEquals(0, $violations->count());
    }

    /**
     * Test validation cost
     */
    public function testCostValidation()
    {
        $product = new Product();

        $validator = $this->container->get('validator');
        /** @var \Symfony\Component\Validator\ConstraintViolationList $violations */
        $violations = $validator->validateProperty($product, 'cost');

        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("This value should not be blank.", $violations->get(0)->getMessage());

        $product->setCost(-1.0);
        $violations = $validator->validateProperty($product, 'cost');
        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("This value should be greater than or equal to 0.", $violations->get(0)->getMessage());

        $product->setCost(1001.0);
        $violations = $validator->validateProperty($product, 'cost');
        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("This value should be less than or equal to 1000.", $violations->get(0)->getMessage());

        $product->setCost("a");
        $violations = $validator->validateProperty($product, 'cost');
        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("This value should be of type numeric.", $violations->get(0)->getMessage());

        $product->setCost(3.3);
        $violations = $validator->validateProperty($product, 'cost');
        $this->assertEquals(0, $violations->count());

    }


    /**
     * Test validation stock count
     */
    public function testStockCountValidation()
    {
        $product = new Product();

        $validator = $this->container->get('validator');
        /** @var \Symfony\Component\Validator\ConstraintViolationList $violations */
        $violations = $validator->validateProperty($product, 'stockCount');

        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("This value should not be blank.", $violations->get(0)->getMessage());

        $product->setStockCount(-1);
        $violations = $validator->validateProperty($product, 'stockCount');
        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("This value should be greater than or equal to 0.", $violations->get(0)->getMessage());

        $product->setStockCount("a");
        $violations = $validator->validateProperty($product, 'stockCount');
        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("This value should be of type numeric.", $violations->get(0)->getMessage());

        $product->setStockCount(1);
        $violations = $validator->validateProperty($product, 'stockCount');
        $this->assertEquals(0, $violations->count());

    }

    /**
     * Test validation cost and stock count
     */
    public function testCostAndStockValidation()
    {
        $product = new Product();

        $validator = $this->container->get('validator');
        /** @var \Symfony\Component\Validator\ConstraintViolationList $violations */

        $product->setCode("P001");
        $product->setName("Name");
        $product->setDescription("Description");
        $product->setStockCount(1);
        $product->setCost(3.1);

        $violations = $validator->validate($product);
        $this->assertGreaterThan(0, $violations->count());
        $this->assertSame("The stock count and cost are too low", $violations->get(0)->getMessage());

        $product->setStockCount(10);
        $product->setCost(3.1);

        $violations = $validator->validate($product);
        $this->assertEquals(0, $violations->count());

        $product->setCost(5.1);
        $product->setStockCount(1);

        $violations = $validator->validate($product);
        $this->assertEquals(0, $violations->count());

    }

}
