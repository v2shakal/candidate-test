<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="product")
 * @UniqueEntity("code")
 */
class Product
{
    const MIN_IMPORT_COST_IN_COMPLEX_RULE = 5;
    const MIN_IMPORT_STOCK_COUNT_IN_COMPLEX_RULE = 10;
    const MAX_IMPORT_COST = 1000;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=10, unique=true, nullable=false)
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=10)
     */
    protected $code;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    protected $description;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true, "default":0}))
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="numeric")
     * @Assert\GreaterThanOrEqual(value=0)
     */
    protected $stockCount;

    /**
     * @ORM\Column(type="decimal", precision=18, scale=2)
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="numeric")
     * @Assert\LessThanOrEqual(value=Product::MAX_IMPORT_COST)
     * @Assert\GreaterThanOrEqual(value=0)
     */
    protected $cost;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    protected $addedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $discontinuedAt;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * @Assert\IsTrue(message = "The stock count and cost are too low")
     *
     * @return boolean whether validation passed
     */
    public function isStockCountAndCostValid()
    {
        if ($this->cost < Product::MIN_IMPORT_COST_IN_COMPLEX_RULE
            && $this->stockCount < Product::MIN_IMPORT_STOCK_COUNT_IN_COMPLEX_RULE
        ) {
            return false;
        }

        return true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Product
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set stockCount
     *
     * @param integer $stockCount
     *
     * @return Product
     */
    public function setStockCount($stockCount)
    {
        $this->stockCount = $stockCount;

        return $this;
    }

    /**
     * Get stockCount
     *
     * @return integer
     */
    public function getStockCount()
    {
        return $this->stockCount;
    }

    /**
     * Set cost
     *
     * @param string $cost
     *
     * @return Product
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set addedAt
     *
     * @param \DateTime $addedAt
     *
     * @return Product
     */
    public function setAddedAt($addedAt)
    {
        $this->addedAt = $addedAt;

        return $this;
    }

    /**
     * Get addedAt
     *
     * @return \DateTime
     */
    public function getAddedAt()
    {
        return $this->addedAt;
    }

    /**
     * Set discontinuedAt
     *
     * @param \DateTime $discontinuedAt
     *
     * @return Product
     */
    public function setDiscontinuedAt($discontinuedAt)
    {
        $this->discontinuedAt = $discontinuedAt;

        return $this;
    }

    /**
     * Get discontinuedAt
     *
     * @return \DateTime
     */
    public function getDiscontinuedAt()
    {
        return $this->discontinuedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Product
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

}
