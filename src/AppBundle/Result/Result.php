<?php

namespace AppBundle\Result;

/**
 * Class Result
 * @package AppBundle\Result
 */
class Result
{
    /**
     * @var \Ddeboer\DataImport\Result
     */
    protected $importResult;

    /**
     * @var array
     */
    protected $failedRows = [];


    /**
     * Result constructor.
     * @param \Ddeboer\DataImport\Result $result
     * @param array $failedRows
     */
    public function __construct(\Ddeboer\DataImport\Result $result, array $failedRows = [])
    {
        $this->importResult = $result;
        $this->failedRows = $failedRows;
    }

    /**
     * @return \Ddeboer\DataImport\Result
     */
    public function getImportResult()
    {
        return $this->importResult;
    }

    /**
     * @param \Ddeboer\DataImport\Result $importResult
     */
    public function setImportResult($importResult)
    {
        $this->importResult = $importResult;
    }

    /**
     * @return array
     */
    public function getFailedRows()
    {
        return $this->failedRows;
    }

    /**
     * @param array $failedRows
     */
    public function setFailedRows($failedRows)
    {
        $this->failedRows = $failedRows;
    }
}