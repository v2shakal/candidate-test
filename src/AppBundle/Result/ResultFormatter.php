<?php

namespace AppBundle\Result;

/**
 * Class ResultFormatter
 * @package AppBundle\Result
 */
class ResultFormatter
{
    /**
     * @var Result
     */
    protected $result;

    /**
     * @var array
     */
    protected $propertyMap;

    /**
     * @return Result
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param Result $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * ResultFormatter constructor.
     * @param Result $result
     * @param array $propertyMap
     */
    public function __construct(Result $result, array $propertyMap = [])
    {
        $this->result = $result;
        $this->propertyMap = $propertyMap;
    }

    /**
     * @return array
     */
    public function asConsoleString()
    {
        if ($this->result) {
            $output = [
                'Successfully imported: ' . ($this->result->getImportResult()->getSuccessCount() - count($this->result->getFailedRows())),
                'Failed: ' . count($this->result->getFailedRows()),
            ];

            if ($this->result->getFailedRows()) {
                $output[] = 'Failed rows:';
                $renderHeader = true;
                foreach ($this->result->getFailedRows() as $failedRow) {
                    if ($renderHeader) {
                        $header = [];
                        $flippedPropertyMap = array_flip($this->propertyMap);
                        foreach ($failedRow as $name => $value) {
                            if (isset($flippedPropertyMap[$name])) {
                                $header[] = $flippedPropertyMap[$name];
                            } else {
                                $header[] = $name;
                            }
                        }
                        $output[] = implode(',', $header);
                        $renderHeader = false;
                    }
                    $str = '';
                    foreach ($failedRow as $value) {
                        if ($value instanceof \DateTime) {
                            $str .= 'yes';
                        } else {
                            $str .= (string)$value . ',';
                        }
                    }
                    $output[] = rtrim($str, ',');
                }
            }

            return $output;
        }

        return [];
    }
}