<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HelloWorldCommand extends Command
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "app/console")
            ->setName('app:hello')
            // the short description shown while running "php app/console list"
            ->setDescription('Ouputs Hello {{name}}.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to say hello...")
            // configure an argument
            ->addArgument('name', InputArgument::OPTIONAL, 'Name to Hello.')
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '============',
        ]);

        $name = $input->getArgument('name') ? $input->getArgument('name') : 'World';
        // outputs a message without adding a "\n" at the end of the line
        $output->writeln("Hello $name");

        $output->writeln([
            '============',
        ]);
    }
}