<?php

namespace AppBundle\Command;

use AppBundle\Result\ResultFormatter;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use \SplFileObject;

/**
 * Class ImportCommand
 * @package AppBundle\Command
 */
class ImportCommand extends ContainerAwareCommand
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('app:import')
            ->setDescription('Description will be here')
            ->setHelp('This command allows you to insert records.')
            ->addArgument('filename', InputArgument::REQUIRED, 'Path and filename to csv file')
            ->addArgument('test', InputArgument::OPTIONAL, 'Whether test mode or not.');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Start import',
            '============',
        ]);

        $filename = $input->getArgument('filename');

        if (!is_readable($filename)) {
            $output->writeln([
                '',
                '<error>     ERROR!                </error>',
                '<error>     File not found!       </error>',
                '',
            ]);
            return false;
        }

        $importHelper = $this->getContainer()->get('import_helper');
        $importHelper->setFile(new SplFileObject($filename));
        $importHelper->setTest($input->getArgument('test'));

        $resultFormatter = new ResultFormatter($importHelper->process(), $importHelper->getPropertyMap());
        $output->writeln($resultFormatter->asConsoleString());

        $output->writeln([
            '============',
            'Finish import',
        ]);
    }
}